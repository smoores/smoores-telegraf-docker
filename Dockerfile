FROM telegraf:1.21.2

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends ipmitool smartmontools && \
    rm -rf /var/lib/apt/lists/*
