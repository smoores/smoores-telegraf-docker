# Telegraf Dockerfile

Created by following the instructions under "Install Additional Packages" on the [Telegraf DockerHub page](https://hub.docker.com/_/telegraf/).

This Dockerfile includes steps for installing `ipmitool` and `smartmontools`, so they can be used by the SMART and IPMI plugins for Telegraf.
